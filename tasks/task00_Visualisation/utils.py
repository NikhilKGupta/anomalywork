import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

def plot_prod_vs_dimension(prod_vs_dimesion):
    '''
    Used to plot the dataframe of all the products for each dimension
    '''
    
    for i, key in enumerate(prod_vs_dimesion):
        prod_vs_dimesion[key].plot(grid=True, figsize = (20,4), title = key)
        

def build_query(col_list, val_list):
    '''
    Used to build thw query to filter the dataframe based on column values
    '''
    assert(len(col_list) == len(val_list))
    sub_query_list = ['{} == "{}"'.format(col_list[idx], val_list[idx]) for idx in range(len(col_list))]
    query = ' & '.join(sub_query_list)
    
    return str(query)


def create_product_vs_dimesion_grp(input_df, prod_list, dimension_list, dimension_name):
    '''
    Create the dataframe of NBRx for all the product for each dimension
    '''
    
    col_list = ['Product', dimension_name]
    product_vs_dimension = {}
    
    for dimension in dimension_list:
        product_vs_dimension_nbrx = pd.DataFrame()
        for prod in prod_list:
            val_list = [prod, dimension]
            query = build_query(col_list, val_list)
            product_vs_dimension_nbrx[prod] = input_df.query(query).groupby('timestamp')['NBRx'].sum()
        
        product_vs_dimension_nbrx[dimension_name + '_NBRx'] = product_vs_dimension_nbrx.sum(axis=1)
        product_vs_dimension[dimension] = product_vs_dimension_nbrx
    return product_vs_dimension

